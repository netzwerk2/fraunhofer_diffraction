use std::num::NonZeroU32;
use std::sync::{Arc, Mutex};

use fast_image_resize::{FilterType, Image, PixelType, ResizeAlg, Resizer};
use image::{ImageBuffer, Rgb};
use palette::{FromColor, Srgb, Xyz};
use rayon::prelude::*;
use rustfft::num_complex::Complex;
use rustfft::{Fft, FftPlanner};
use toodee::{CopyOps, TooDeeOps, TooDeeOpsMut, TranslateOps};

use crate::texture::TextureProperties;
use crate::{Texture, WAVELENGTH_TO_XYZ};

/// The struct for calculating the 2D Fourier transform of an image.
// TODO: Should the struct really have ownership of the Texture?
#[derive(Clone)]
pub struct Fourier2D<T: Default> {
    pub signal: Texture<Complex<T>>,
    horizontal_fft: Arc<dyn Fft<T>>,
    vertical_fft: Arc<dyn Fft<T>>,
}

impl Fourier2D<f32> {
    /// Create a new Fourier2D.
    pub fn new(signal: Texture<Complex<f32>>) -> Self {
        let mut fft_planner = FftPlanner::new();
        let horizontal_fft = fft_planner.plan_fft_forward(signal.width());
        let vertical_fft = fft_planner.plan_fft_forward(signal.height());

        Self {
            signal,
            horizontal_fft,
            vertical_fft,
        }
    }

    /// Calculate the Fourier transform in x-direction.
    fn process_horizontal(&self, destination: &mut Texture<Complex<f32>>) {
        let Self { horizontal_fft, .. } = self;

        let (width, height) = destination.dimensions();

        destination.pixels.rows_mut().for_each(|row| {
            horizontal_fft.process(row);
        });

        (0..height).for_each(|y| {
            destination
                .pixels
                .view_mut((0, y), (width, y + 1))
                .translate_with_wrap((width / 2, 0));
        });
    }

    /// Calculate the Fourier transform in y-direction.
    fn process_vertical(&self, destination: &mut Texture<Complex<f32>>) {
        let Self { vertical_fft, .. } = self;

        let (width, height) = destination.dimensions();

        (0..width).for_each(|x| {
            let mut column = destination
                .pixels
                .col(x)
                .copied()
                .collect::<Vec<Complex<f32>>>();

            vertical_fft.process(&mut column);

            column.rotate_right(height / 2);

            destination
                .pixels
                .view_mut((x, 0), (x + 1, height))
                .copy_from_slice(&column);
        });
    }

    fn scale(
        &self,
        destination: &mut Texture<Complex<f32>>,
        scale_factor: (f32, f32),
        texture_properties: TextureProperties,
    ) {
        let (width, height) = (
            NonZeroU32::new(destination.width() as u32).unwrap(),
            NonZeroU32::new(destination.height() as u32).unwrap(),
        );
        let (dst_width, dst_height) = (
            NonZeroU32::new((scale_factor.0 * destination.width() as f32) as u32).unwrap(),
            NonZeroU32::new((scale_factor.1 * destination.height() as f32) as u32).unwrap(),
        );

        let image = Image::from_vec_u8(
            width,
            height,
            destination.to_image_buffer(texture_properties).into_raw(),
            PixelType::U8x3,
        )
        .unwrap();

        if width == dst_width && height == dst_height {
            *destination = Texture::from_generic_image(
                &ImageBuffer::<Rgb<u8>, _>::from_raw(width.into(), width.into(), image.buffer())
                    .unwrap(),
            );

            return;
        }

        let mut dst_image = Image::new(dst_width, dst_height, image.pixel_type());

        let mut dst_view = dst_image.view_mut();

        let mut resizer = Resizer::new(ResizeAlg::Convolution(FilterType::CatmullRom));
        resizer.resize(&image.view(), &mut dst_view).unwrap();

        let resized_signal = Texture::from_generic_image(
            &ImageBuffer::<Rgb<u8>, _>::from_raw(
                dst_width.into(),
                dst_height.into(),
                dst_image.buffer(),
            )
            .unwrap(),
        );

        let (width, height) = (width.get() as usize, height.get() as usize);
        let (dst_width, dst_height) = (dst_width.get() as usize, dst_height.get() as usize);

        destination.pixels.fill(Complex::default());

        if dst_width < width && dst_height < height {
            let width_start = (width - dst_width) / 2;
            let height_start = (height - dst_height) / 2;

            let mut signal_view = destination.pixels.view_mut(
                (width_start, height_start),
                (width_start + dst_width, height_start + dst_height),
            );
            signal_view
                .rows_mut()
                .zip(resized_signal.pixels.rows())
                .for_each(|(signal_rows, resized_rows)| signal_rows.copy_from_slice(resized_rows));
        } else {
            let width_start = (dst_width - width) / 2;
            let height_start = (dst_height - height) / 2;

            let resized_view = resized_signal.pixels.view(
                (width_start, height_start),
                (width_start + width, height_start + height),
            );
            destination
                .pixels
                .rows_mut()
                .zip(resized_view.rows())
                .for_each(|(signal_rows, resized_rows)| signal_rows.copy_from_slice(resized_rows));
        }
    }

    /// Calculates the 2D Fourier transform of the signal,
    /// by first processing horizontally and then vertically.
    fn process(
        &self,
        destination: &mut Texture<Complex<f32>>,
        scale_factor: (f32, f32),
        texture_properties: TextureProperties,
    ) {
        self.process_horizontal(destination);
        self.process_vertical(destination);

        self.scale(destination, scale_factor, texture_properties);
    }

    // TODO: Panic or ensure quadratic images -> simplify aperture_pixel_size etc.
    pub fn fraunhofer_diffraction(
        &self,
        screen_distance: f32,
        wavelengths: &[f32],
        aperture_pixel_size: (f32, f32),
        texture_properties: TextureProperties,
    ) -> Texture<Srgb> {
        let output = Arc::new(Mutex::new(Texture::<Srgb>::new(
            self.signal.width(),
            self.signal.height(),
        )));

        wavelengths
            .par_iter()
            .enumerate()
            .for_each(|(i, wavelength)| {
                let output = Arc::clone(&output);

                let screen_pixel_weight = wavelength * screen_distance;
                let screen_pixel_size = (
                    screen_pixel_weight / (self.signal.width() as f32 * aperture_pixel_size.0),
                    screen_pixel_weight / (self.signal.height() as f32 * aperture_pixel_size.1),
                );

                let scale_factor = (
                    screen_pixel_size.0 / aperture_pixel_size.0,
                    screen_pixel_size.1 / aperture_pixel_size.1,
                );

                let mut processed = self.signal.clone();

                self.process(&mut processed, scale_factor, texture_properties);

                processed.pixels.into_iter().enumerate().for_each(|(j, c)| {
                    let value = c.norm();

                    let xyz = Xyz::from(WAVELENGTH_TO_XYZ[i]);
                    let srgb = Srgb::from_color(xyz) * value;

                    output.lock().unwrap().pixels.data_mut()[j] += srgb;
                })
            });

        Arc::try_unwrap(output).unwrap().into_inner().unwrap()
    }
}
